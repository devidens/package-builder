# haproxy with static OpenSSL linking and TLS v1.3
This provides haproxy RPM spec file and required files e.g. systemd service file, etc. to build RPM for CentOS 7.

## How to build RPM
You can set OpenSSL version number in the Dockerfile.

If you have a docker environment, you can build RPMs by just running

    make

If you'd like to build RPM for specific distribution, please run a command like following

    make centos7

Now this handles *centos7* only.

To build RPM in your server without docker, please copy files under rpmbuild to your build system and install build dependencies.

## Installing RPM
After building, please copy RPM under \*.build directory to your system and run

    yum install haproxy-1.8.17-3.el7.x86_64.rpm

Once the installation finishes successfully, you can see a sample configuration file at /etc/haproxy/haproxy.conf.

To start haproxy, please run

    systemctl enable haproxy.service
    systemctl start haproxy.service

haproxy v1.8 supports systemd natively, no need systemd-wrapper anymore. You can send SIGUSR2 to reload config without interruption.

## License
This is under MIT License. Please see the LICENSE file for details.
