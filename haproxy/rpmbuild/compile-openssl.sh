#!/bin/sh

set -e

echo "------------------------------------- openssl complie has been started"

cd /tmp

curl -SL http://openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz -o openssl-${OPENSSL_VERSION}.tar.gz

tar xzpf openssl-${OPENSSL_VERSION}.tar.gz

cd openssl-${OPENSSL_VERSION}

./Configure --prefix=/usr/local/openssl zlib no-ssl3 no-ssl3-method no-shared enable-camellia enable-seed enable-rfc3779 enable-cms enable-md2 enable-ec_nistp_64_gcc_128 no-mdc2 no-rc5 no-ec2m no-gost no-srp -DOPENSSL_USE_IPV6=1 linux-x86_64
make depend
make
make install

echo "------------------------------------- openssl has been complied and installed"
